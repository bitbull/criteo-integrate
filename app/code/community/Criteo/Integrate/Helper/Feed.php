<?php
function getParentProductId($product){
   try {
        $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
            ->getParentIdsByChild($product->getId());
        if (sizeof($parentIds) > 0) {
            return $parentIds[0];
        }
    } catch (Exception $e) {}
    return NULL;
}

class Criteo_Integrate_Helper_Feed extends Mage_Core_Helper_Abstract
{
    public function getFeed($limit, $page)
    {
        $feed = $this->createFeed();
        $productCollection = $this->getProductCollection();

        $totalProducts = $productCollection->getSize();

        if ((($page - 1) * $limit) >= $totalProducts)
            return $feed->asXML();

        $fetchedProducts = array();
        foreach ($productCollection->setPageSize($limit)->setCurPage($page) as $product) {
            array_push($fetchedProducts, $product);
        }

        $parentProductsById = $this->getParentProductIndex($fetchedProducts);
        foreach($fetchedProducts as $product){
            $parentProductId = getParentProductId($product);
            $parentProduct = array_key_exists($parentProductId, $parentProductsById) ? $parentProductsById[$parentProductId] : null;
            $feed->addProduct($product, $parentProduct);
        }

        return $feed->asXML();
    }

    private function createFeed()
    {
        $feed = Mage::getModel("Criteo_Integrate_Feed/template");
        $feed->setTitle(Mage::getStoreConfig('design/head/default_title'));
        $feed->setLink(Mage::getBaseUrl());
        $feed->setDescription(Mage::getStoreConfig('design/head/default_description'));
        return $feed;
    }

    private function getProductCollection()
    {
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addAttributeToFilter('type_id', array('eq' => 'simple'));
        $productCollection->addWebsiteFilter();
        $productCollection->addAttributeToFilter(
            'status',
            array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
        );
        return $productCollection;
    }

    private function getParentProductIndex($productCollection) {
        $parentProductCollection = $this->getParentProductCollection($productCollection);
        $parentProductsById = array();
        foreach($parentProductCollection as $parentProduct){
            $parentProductsById[$parentProduct->getId()] = $parentProduct;
        }
        return $parentProductsById;
    }

    private function getParentProductCollection($products){
        $parentIds = array_unique(array_map(getParentProductId, $products));

        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addAttributeToFilter('entity_id', array('in' => $parentIds));
        $productCollection->addWebsiteFilter();
        return $productCollection;
    }
}
