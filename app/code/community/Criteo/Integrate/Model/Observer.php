<?php

class Criteo_Integrate_Model_Observer
{
    const MAX_PRODUCTS = 3;
    private $ajaxAlreadyInjected = false;
    private $locationHelper;

    public function __construct()
    {
        $this->locationHelper = Mage::helper('Criteo_Integrate/location');
    }

    public function onCoreBlockAbstractToHtmlAfter($observer)
    {
        try {
            if (!$this->isAjaxRequest() || $this->ajaxAlreadyInjected)
                return;

            $block = $observer->getEvent()->getBlock();
            if ($this->isListingPage() && ($block instanceof Mage_Catalog_Block_Product_List)) {
                $this->injectAjaxUpdate($observer->getTransport(), $this->getListingEventJson());
            }
        } catch (Exception $e) {
        }
    }

    public function onControllerActionLayoutLoadBefore($observer)
    {
        try {
            $layout = $observer->getEvent()->getLayout();
            $layout->getUpdate()->addHandle('criteo_onetag');
        } catch (Exception $e) {
        }
    }

    public function getPartnerId()
    {
        return Mage::helper('Criteo_Integrate')->getPartnerId();
    }

    public function shouldInjectOneTag()
    {
        if ($this->isHomePage()) {
            return true;
        }

        if ($this->isProductPage()) {
            return true;
        }

        if ($this->isListingPage()) {
            $productIds = $this->getListingProductIds();
            return !empty($productIds);
        }

        if ($this->isCartPage()) {
            $cart = $this->getCartProducts();
            return !empty($cart);
        }

        return $this->isSalesConfirmationPage();
    }

    public function isHomePage()
    {
        return $this->locationHelper->isHomePage();
    }

    public function isProductPage()
    {
        return $this->locationHelper->isProductPage();
    }

    public function isListingPage()
    {
        return $this->locationHelper->isListingPage();
    }

    public function isCartPage()
    {
        return $this->locationHelper->isCartPage();
    }

    public function isSalesConfirmationPage()
    {
        return $this->locationHelper->isSalesConfirmationPage();
    }

    public function isAdvancedSearchResultPage()
    {
        return $this->locationHelper->isAdvancedSearchResultPage();
    }

    public function getHomeEventJson()
    {
        return json_encode(array("event" => "viewHome"));
    }

    public function getProductEventJson()
    {
        return json_encode(array("event" => "viewItem", "item" => $this->getCurrentProductId()));
    }

    public function getListingEventJson()
    {
        return json_encode(array("event" => "viewList", "item" => $this->getListingProductIds()));
    }

    public function getCartEventJson()
    {
        return json_encode(array("event" => "viewBasket", "item" => $this->getCartProducts()));
    }

    public function getSalesConfirmationEventJson()
    {
        return json_encode(array(
            "event" => "trackTransaction",
            "item" => $this->getTransactionProducts(),
            "id" => $this->getTransactionId()
        ));
    }

    public function getCustomerHashedEmail()
    {
        $session = Mage::getSingleton('customer/session');

        if (!$session->isLoggedIn()) {
            return null;
        }

        $customer = $session->getCustomer();
        if (!$customer) {
            return null;
        }

        $email = $customer->getEmail();
        $sanitizedEmail = trim(strtolower($email));

        if ($sanitizedEmail == "") {
            return null;
        }

        return $this->getSha256($this->getMd5($sanitizedEmail));
    }

    private function getCurrentProductId()
    {
        if (!$this->isProductPage()) {
            return null;
        }

        return $this->getProductIdentifier($this->getProduct());
    }

    private function getListingProductIds()
    {
        $productIds = array();

        try {
            if (!$this->isListingPage() && !$this->isAdvancedSearchResultPage()) {
                return $productIds;
            }

            if ($this->isAdvancedSearchResultPage()) {
                $productCollection = Mage::getSingleton('catalogsearch/advanced')->getProductCollection();
            } else {
                $catalogProductList = Mage::getBlockSingleton('catalog/product_list');
                if (!isset($catalogProductList)) {
                    return $productIds;
                }
                $productCollection = $catalogProductList->getLoadedProductCollection();
            }

            if (!isset($productCollection)) {
                return $productIds;
            }
            if ($productCollection->count() == 0) {
                return $productIds;
            }

            foreach ($productCollection as $product) {
                array_push($productIds, $this->getProductIdentifier($product));
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return array_slice($productIds, 0, self::MAX_PRODUCTS);
    }

    private function getCartProducts()
    {
        try {
            $checkoutSession = $this->getCheckoutSession();

            if (!isset($checkoutSession)) {
                return array();
            }

            $items = $checkoutSession->getQuote()->getAllItems();
            if (!isset($items)) {
                $cartHelper = Mage::helper('checkout/cart');
                $items = $cartHelper->getCart()->getItems();
            }

            return $this->productItemsToTrackersInfo($items);
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return array();
    }

    private function getTransactionProducts()
    {
        try {
            $order = $this->getCurrentOrder();

            if (!isset($order)) {
                return array();
            }

            return $this->productItemsToTrackersInfo($order->getAllItems());
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return array();
    }

    private function getTransactionId()
    {
        try {
            $order = $this->getCurrentOrder();

            if (!isset($order)) {
                return;
            }

            return $order->getIncrementId();
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return;
    }

    private function getProductIdentifier($product)
    {
        if (Mage::helper('Criteo_Integrate')->shouldUseSku()) {
            return $product->getSku();
        } else {
            return $product->getId();
        }
    }

    private function getCurrentOrder()
    {
        $checkoutSession = $this->getCheckoutSession();

        if (!isset($checkoutSession)) {
            return;
        }

        $orderId = $checkoutSession->getLastOrderId();

        if (!isset($orderId)) {
            return;
        }

        return Mage::getModel('sales/order')->load($orderId);
    }

    private function productItemsToTrackersInfo($items)
    {
        if (!isset($items)) {
            return array();
        }

        $products = array();
        foreach ($this->deduplicateSimpleAndConfigurableProductItems($items) as $item) {
            array_push($products, $this->getItemTrackersInfo($item, $item->getParentItem()));
        }

        return $products;
    }

    /*
     * Magento products can be either Simple Products either Configurable Products.
     * Basically Simple Products inherit from Configurable Products some properties,
     * but override others, such as SKU, Product Id or price.
     *
     * `getAllItems` will yield both the Simple Product and its parent Configurable
     * Product, so we need to get rid of the Configurable Products that also have children
     * in order to avoid duplicates.
     */
    private function deduplicateSimpleAndConfigurableProductItems($items)
    {
        $simpleProducts = $this->getSimpleProductItems($items);
        $configurableProducts = $this->getConfigurableProductItems($items);

        $simpleProductsParentsHashSet = new SplObjectStorage();
        foreach ($simpleProducts as $simpleProduct) {
            $simpleProductsParentsHashSet->attach($simpleProduct->getParentItem());
        }

        $relevantItems = $simpleProducts;
        foreach ($configurableProducts as $configurableProduct) {
            if (!$simpleProductsParentsHashSet->contains($configurableProduct)) {
                $relevantItems[] = $configurableProduct;
            }
        }

        return $relevantItems;
    }

    private function getSimpleProductItems($items) {
        return array_filter($items, function ($item) {
            $parentItem = $item->getParentItem();
            return isset($parentItem);
        });
    }

    private function getConfigurableProductItems($items) {
        return array_filter($items, function ($item) {
            $parentItem = $item->getParentItem();
            return !isset($parentItem);
        });
    }

    private function getItemTrackersInfo($item, $parentItem = null)
    {
        $productId = $item->getProductId();
        $product = $this->getProductById($productId);

        $parentProduct = null;
        if (!is_null($parentItem))
            $parentProduct = $this->getProductById($parentItem->getProductId());

        $productWrapper = Mage::getModel('Criteo_Integrate_Feed/productWrapper');
        $productWrapper->setProducts($product, $parentProduct);

        $quantity = $this->getItemQuantity(is_null($parentItem) ? $item : $parentItem);

        return array(
            "id" => $productWrapper->getProductId(),
            "price" => (float)$productWrapper->getProductSalePrice(),
            "quantity" => floatval($quantity)
        );
    }

    private function getItemQuantity($item)
    {
        $quantity = $item->getQty();
        // For orders, quantity is set differently
        if (!isset($quantity)) {
            $quantity = $item->getQtyOrdered();
        }
        return $quantity;
    }

    private function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    private function getProduct()
    {
        return Mage::registry('current_product');
    }

    private function getProductById($productId)
    {
        return Mage::getModel('catalog/product')->load($productId);
    }

    private function getRequest()
    {
        return Mage::app()->getFrontController()->getRequest();
    }

    private function getMd5($emailString)
    {
        return hash("md5", $emailString);
    }

    private function getSha256($emailMd5)
    {
        return hash("sha256", $emailMd5);
    }

    private function injectAjaxUpdate($transport, $eventJson)
    {
        $html = $transport->getHtml();
        $transport->setHtml($html.'<script type="text/javascript">window.criteo_magento_event('.$eventJson.', '.json_encode(time()).')</script>');
        $this->ajaxAlreadyInjected = true;
    }

    private function isAjaxRequest() {
        return $this->getRequest()->isXmlHttpRequest();
    }
}
