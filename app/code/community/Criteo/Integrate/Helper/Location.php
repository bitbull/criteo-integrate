<?php

class Criteo_Integrate_Helper_Location extends Mage_Core_Helper_Abstract
{
    private $moduleNameLocator;
    private $urlPathLocator;

    public function __construct()
    {
        $this->moduleNameLocator = Mage::helper('Criteo_Integrate_Location/locateWithModuleNameStrategy');
        $this->urlPathLocator = Mage::helper('Criteo_Integrate_Location/locateWithUrlPathStrategy');
    }

    public function isHomePage()
    {
        return $this->urlPathLocator->isHomePage();
    }

    public function isProductPage()
    {
        $product = Mage::registry('current_product');
        return isset($product);
    }

    public function isListingPage()
    {
        return $this->moduleNameLocator->isListingPage();
    }

    public function isCartPage()
    {
        return $this->moduleNameLocator->isCartPage() ||
               $this->urlPathLocator->isCartPage();
    }

    public function isSalesConfirmationPage()
    {
        return $this->moduleNameLocator->isSalesConfirmationPage();
    }

    public function isAdvancedSearchResultPage()
    {
        return $this->moduleNameLocator->isAdvancedSearchResultPage();
    }
}
