<?php

class Criteo_Integrate_IntegrateController extends Mage_Core_Controller_Front_Action
{
    const FEED_PER_PAGE_LIMIT = 1000;
    private $feedHelper;
    private $authenticationHelper;

    protected function _construct()
    {
        $this->feedHelper = Mage::helper('Criteo_Integrate/feed');
        $this->authenticationHelper = Mage::helper('Criteo_Integrate/authentication');
    }

    /*
     * GET `/criteo/integrate/version`
     */
    public function versionAction()
    {
        $helper = Mage::helper('Criteo_Integrate');
        $this->getResponse()
            ->setBody($helper->getExtensionVersion());
    }

    /*
     * GET `/criteo/integrate/info`
     */
    public function infoAction()
    {
        if (!$this->authenticationHelper->checkIsAuthenticated()) {
            exit;
        }

        $parameters = $this->getRequest()->getParams();
        $withPatches = array_key_exists("patches", $parameters);
        $withExtensions = array_key_exists("extensions", $parameters);

        $helper = Mage::helper('Criteo_Integrate');
        $this->json($helper->getInfo($withPatches, $withExtensions));
    }

    /*
     * GET `/criteo/integrate/feed`
     */
    public function feedAction()
    {
        if (!$this->authenticationHelper->checkIsAuthenticated()) {
            exit;
        }

        $parameters = $this->getRequest()->getParams();

        $limit = 250;
        if (array_key_exists('limit', $parameters)) {
            $limit = min(max(1, intval($parameters['limit'])), self::FEED_PER_PAGE_LIMIT);
        }

        $page = 1;
        if (array_key_exists('page', $parameters)) {
            $page = max($page, intval($parameters['page']));
        }

        $this->xml($this->feedHelper->getFeed($limit, $page));
    }

    private function xml($data)
    {
        $this->getResponse()
            ->setHeader('Content-type', 'application/xml');
        $this->getResponse()
            ->setBody($data);
    }

    private function json($object)
    {
        $this->getResponse()
            ->setHeader('Content-type', "application/json");
        if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
            $this->getResponse()
                ->setBody(json_encode($object, JSON_PRETTY_PRINT));
        } else {
            $this->getResponse()
                ->setBody(json_encode($object));
        }
    }
}
