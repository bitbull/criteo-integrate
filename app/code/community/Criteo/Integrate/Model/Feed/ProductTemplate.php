<?php

class Criteo_Integrate_Model_Feed_ProductTemplate extends Mage_Core_Model_Abstract
{
    const TAG_NAME_PREFIX = 'g:';

    private $node;

    public function __construct($node)
    {
        $this->node = $node;
    }

    public function setProduct($product, $parentProduct)
    {
        $productWrapper = Mage::getModel('Criteo_Integrate_Feed/productWrapper');
        $productWrapper->setProducts($product, $parentProduct);

        $this->addAttribute('id', $productWrapper->getProductId());
        $this->addAttribute('title', $productWrapper->getProductTitle());
        $this->addAttribute('description', $productWrapper->getProductDescription());
        $this->addAttribute('price', $productWrapper->getProductPrice());
        $this->addAttribute('sale_price', $productWrapper->getProductSalePrice());
        $this->addAttribute('link', $productWrapper->getProductLink());
        $this->addAttribute('image_link', $productWrapper->getImageLink());
        $this->addAttribute('additional_image_link', $productWrapper->getSmallImageLink());
        $this->addAttribute('availability', $productWrapper->getProductAvailability());
        $this->addAttributeIfNotNull('item_group_id', $productWrapper->getProductItemGroupId());
        $this->addAttributeIfNotNull('number_of_reviews', $productWrapper->getProductNumberOfReviews());
        $this->addAttributeIfNotNull('product_rating', $productWrapper->getProductRating());
        $this->addAttributeIfNotEmpty('product_type', $productWrapper->getProductCategories());
    }

    private function addAttributeIfNotEmpty($attributeName, $attributeValue)
    {
        if (sizeof($attributeValue) > 0) {
            $this->addAttribute($attributeName, $attributeValue);
        }
    }

    private function addAttributeIfNotNull($attributeName, $attributeValue)
    {
        if (!is_null($attributeValue)) {
            $this->addAttribute($attributeName, $attributeValue);
        }
    }

    private function addAttribute($attributeName, $attributeValue)
    {
        $tagName = self::TAG_NAME_PREFIX . self::TAG_NAME_PREFIX . $attributeName;
        $this->node->addChild($tagName, $attributeValue);
    }
}
