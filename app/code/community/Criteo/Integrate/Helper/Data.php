<?php

class Criteo_Integrate_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SETTINGS_PARTNER_ID = 'Criteo_Integrate/settings/partner_id';
    const SETTINGS_MOAB_ID = 'Criteo_Integrate/settings/moab_id';
    const SETTINGS_USE_SKU = 'Criteo_Integrate/settings/use_sku';
    const SETTINGS_PASSWORD = 'Criteo_Integrate/settings/password';
    const SETTINGS_LOGIN = 'Criteo_Integrate/settings/login';

    public function getPartnerId($store = null)
    {
        return Mage::getStoreConfig(self::SETTINGS_PARTNER_ID, $store);
    }

    public function getLogin($store = null)
    {
       return Mage::getStoreConfig(self::SETTINGS_LOGIN, $store);
    }

    public function getPassword($store = null)
    {
        return Mage::getStoreConfig(self::SETTINGS_PASSWORD, $store);
    }

    public function shouldUseSku($store = null)
    {
        return Mage::getStoreConfigFlag(self::SETTINGS_USE_SKU, $store);
    }

    public function shouldUseSimpleProductPrice()
    {
        // SimpleProductPricing will use simple product price over configurable product price
        // Here we check that the extension is installed and that it is enabled
        return is_dir(Mage::getBaseDir('code') . '/local/Ayasoftware/SimpleProductPricing') && Mage::getStoreConfig('spp/setting/enableModule');
    }

    public function getInfo($withPatches, $withExtensions)
    {
        $result = array(
            "partnerId" => $this->getPartnerId(),
            "magentoVersion" => $this->getMagentoVersion(),
            "extensionVersion" => $this->getExtensionVersion(),
            "moabId" => $this->getMoabId()
        );

        if ($withPatches) {
            $result["patches"] = $this->getPatches();
        }

        if ($withExtensions) {
            $result["extensions"] = $this->getInstalledExtensions();
        }

        return $result;
    }

    public function getExtensionVersion()
    {
        return (string)Mage::getConfig()->getNode()->modules->Criteo_Integrate->version;
    }

    private function getMagentoVersion()
    {
        return Mage::getVersion();
    }

    private function getMoabId($store = null)
    {
        return Mage::getStoreConfig(self::SETTINGS_MOAB_ID, $store);
    }

    private function getPatches()
    {
        return Mage::getModel('Criteo_Integrate/patches')->getPatches();
    }

    private function getInstalledExtensions()
    {
        $extensions = array();

        foreach (Mage::getConfig()->getNode('modules')->children() as $moduleName => $moduleConfig) {
            $extensions[$moduleName] = (string)$moduleConfig->version;
        }

        return $extensions;
    }
}
