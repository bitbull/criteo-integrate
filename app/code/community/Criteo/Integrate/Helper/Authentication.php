<?php

/**
 * Helper for HTTP Digest Authentication.
 * Implementation taken and adapted from: http://php.net/manual/en/features.http-auth.php
 */
class Criteo_Integrate_Helper_Authentication extends Mage_Core_Helper_Abstract
{
    const TOKEN_HEADER = 'HTTP_X_CRITEO_TOKEN';

    public function checkIsAuthenticated()
    {
        if (!$this->hasAuthenticationHeader()) {
            header('HTTP/1.1 401 Unauthorized');
            header('X-Criteo-Token: Criteo Token is missing');
            return false;
        }

        if (!$this->checkAuthentication()) {
            header('HTTP/1.0 401 Unauthorized');
            return false;
        }

        return true;
    }

    private function hasAuthenticationHeader()
    {
        return isset($_SERVER[self::TOKEN_HEADER]) && $_SERVER[self::TOKEN_HEADER] !== '';
    }

    private function checkAuthentication()
    {
        $data = base64_decode($_SERVER[self::TOKEN_HEADER]);
        $userAndPassword = explode(":", $data, 2);
        return $userAndPassword[0] == $this->getUser() && $userAndPassword[1] == $this->getPassword();
    }

    private function getUser()
    {
        return Mage::helper('Criteo_Integrate')->getLogin();
    }

    private function getPassword()
    {
        return Mage::helper('Criteo_Integrate')->getPassword();
    }
}
