<?php

class Criteo_Integrate_Helper_Location_LocateWithUrlPathStrategy
{
    public function isHomePage()
    {
        return Mage::app()->getRequest()->getRequestString() == "/";
    }

    public function isCartPage()
    {
        return strpos(Mage::app()->getRequest()->getRequestString(), "/checkout/cart") === 0;
    }
}
