<?php

class Criteo_Integrate_Model_Feed_ProductWrapper extends Mage_Core_Model_Abstract
{
    private $product;
    private $parentProduct;
    private $reviewSummary;
    private $categoryNames;
    private $helper;
    private $relativePriceAdjustmentInPercents;
    private $absolutePriceAdjustment;

    public function _construct()
    {
        $this->helper = Mage::helper('Criteo_Integrate');
    }

    public function setProducts($product, $parentProduct)
    {
        $this->product = $product;
        $this->parentProduct = $parentProduct;
        $this->reviewSummary = $this->fetchReviewSummary();
        $this->categoryNames = $this->fetchCategoryNames();

        $this->computePriceAdjustmentsFromAttributes();
    }

    public function getProductId()
    {
        return $this->getIdOrSku($this->product);
    }

    public function getProductTitle()
    {
        return htmlspecialchars($this->product->getName());
    }

    public function getProductDescription()
    {
        return htmlspecialchars($this->product->getDescription());
    }
    
    public function getProductPrice()
    {
       if ($this->shouldUseSimpleProductPrice()){
           return $this->formatPrice($this->getPriceWithAdjustments($this->product->getPrice()));
       }
       return $this->formatPrice($this->getPriceWithAdjustments($this->parentProduct->getPrice()));
    }

    public function getProductSalePrice()
    {
       if ($this->shouldUseSimpleProductPrice()){
           return $this->formatPrice($this->getPriceWithAdjustments($this->product->getFinalPrice()));
       }
       return $this->formatPrice($this->getPriceWithAdjustments($this->parentProduct->getFinalPrice()));
    }

    public function getProductLink()
    {
        if (is_null($this->parentProduct)) {
            return $this->product->getProductUrl();
        }
        return $this->parentProduct->getProductUrl();
    }

    public function getSmallImageLink()
    {
      $baseProduct = is_null($this->parentProduct) ? $this->product : $this->parentProduct;

      return $baseProduct->getImageUrl();
    }

    public function getImageLink()
    {
      $baseProduct = is_null($this->parentProduct) ? $this->product : $this->parentProduct;

      return Mage::getModel('catalog/product_media_config')->getMediaUrl($baseProduct->getImage());
    }

    public function getProductAvailability()
    {
        if ($this->product->getStockItem()->getIsInStock()) {
            return 'in stock';
        } else {
            return 'out of stock';
        }
    }

    public function getProductItemGroupId()
    {
        if (is_null($this->parentProduct)) {
            return;
        }
        return $this->getIdOrSku($this->parentProduct);
    }

    public function getProductNumberOfReviews()
    {
        if (is_null($this->reviewSummary)) {
            return;
        }
        return $this->reviewSummary->getReviewsCount();
    }

    public function getProductRating()
    {
        if (is_null($this->reviewSummary)) {
            return;
        }
        return $this->reviewSummary->getRatingSummary();
    }

    public function getProductCategories()
    {
        return $this->categoryNames;
    }

    public function shouldUseSimpleProductPrice()
    {
        return is_null($this->parentProduct) || ($this->helper->shouldUseSimpleProductPrice()
                && $this->product->isVisibleInCatalog() && $this->product->isVisibleInSiteVisibility());
    }

    private function computePriceAdjustmentsFromAttributes()
    {
        $this->relativePriceAdjustmentInPercents = 0;
        $this->absolutePriceAdjustment = 0;

        if (!$this->parentProduct) {
            return;
        }

        $configurableAttributes = $this->parentProduct->getTypeInstance(true)->getConfigurableAttributes($this->parentProduct);
        foreach ($configurableAttributes as $att) {
            $attributeValue = $this->product->getData($att->getProductAttribute()->getAttributeCode());
            $prices = $att->getPrices();
            foreach ($prices as $price){
                if ($price['value_index'] == $attributeValue) {
                    if ($price['is_percent']){
                        $this->relativePriceAdjustmentInPercents += (float)$price['pricing_value'];
                    }
                    else {
                        $this->absolutePriceAdjustment += (float)$price['pricing_value'];
                    }
                }
            }
        }
    }

    private function getPriceWithAdjustments($basePrice) {
        $finalPrice = $basePrice;
        $finalPrice += ($this->relativePriceAdjustmentInPercents * $finalPrice) / 100;
        $finalPrice += $this->absolutePriceAdjustment;
        return $finalPrice;
    }

    private function getIdOrSku($product)
    {
        if ($this->helper->shouldUseSku()) {
            return $product->getSku();
        } else {
            return $product->getId();
        }
    }

    private function fetchReviewSummary()
    {
        try {
            $reviewableProduct = $this->parentProduct;
            if (is_null($reviewableProduct)) {
                $reviewableProduct = $this->product;
            }
            return Mage::getModel('review/review_summary')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($reviewableProduct->getId());
        } catch (Exception $e) {}
        return;
    }

    private function fetchCategoryNames()
    {
        try {
            $categoryIds = $this->product->getCategoryIds();
            $categoryId = end($categoryIds);
            $categories = array();
            if ($categoryId != false) {
                $categoryProduct = Mage::getModel('catalog/category')->load($categoryId);
                while ($categoryProduct->getLevel() != 1) {
                    array_unshift($categories, htmlspecialchars($categoryProduct->getName()));
                    $categoryProduct = $categoryProduct->getParentCategory();
                }
            }
            return join(" > ", $categories);
        } catch (Exception $e) {}
        return;
    }

    private function formatPrice($price)
    {
        $currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
        return number_format($price, 2, ".", "") . " " . $currency_code;
    }
}
