<?php

class Criteo_Integrate_Helper_Location_LocateWithModuleNameStrategy
{
    public function isListingPage()
    {
        return (
            $this->isCategoryPage() ||
            $this->isSearchPage() ||
            $this->isTagPage()
        );
    }

    public function isCartPage()
    {
        return (
            $this->isModule('checkout') &&
            $this->isController('cart') &&
            $this->isAction('index')
        );
    }

    public function isSalesConfirmationPage()
    {
        return (
            ($this->moduleContains('checkout') || $this->isController('checkout')) &&
            $this->isAction('success')
        );
    }

    public function isAdvancedSearchResultPage()
    {
        return (
            $this->isController("advanced") &&
            $this->actionContains('result')
        );
    }

    private function isCategoryPage()
    {
        return $this->isController('category') &&
            (Mage::registry('current_category')->getDisplayMode() != Mage_Catalog_Model_Category::DM_PAGE);
    }

    private function isSearchPage()
    {
        return $this->isModule('catalogsearch');
    }

    private function isTagPage()
    {
        return $this->isModule('tag');
    }

    private function moduleContains($name)
    {
        return strpos($this->getRequest()->getModuleName(), $name) !== false;
    }

    private function actionContains($name)
    {
        return strrpos($this->getRequest()->getActionName(), $name) !== false;
    }

    private function isModule($name) {
        return $this->getRequest()->getModuleName() == $name;
    }

    private function isController($name) {
        return $this->getRequest()->getControllerName() == $name;
    }

    private function isAction($name) {
        return $this->getRequest()->getActionName() == $name;
    }

    private function getRequest()
    {
        return Mage::app()->getFrontController()->getRequest();
    }
}
