<?php

class Criteo_Integrate_Model_Patches extends Mage_Core_Model_Abstract
{
    private $appliedPatches = array();
    private $patchFile;

    protected function _construct()
    {
        $this->patchFile = Mage::getBaseDir('etc') . DS . 'applied.patches.list';
        $this->loadPatchFile();
    }

    public function getPatches()
    {
        return $this->appliedPatches;
    }

    private function loadPatchFile()
    {
        $ioAdapter = null;

        try {
            $ioAdapter = new Varien_Io_File();
            if (!$ioAdapter->fileExists($this->patchFile)) {
                return;
            }

            $ioAdapter->open(array('path' => $ioAdapter->dirname($this->patchFile)));
            $ioAdapter->streamOpen($this->patchFile, 'r');
            while ($buffer = $ioAdapter->streamRead()) {
                if (stristr($buffer, '|')) {
                    list($date, $patchName, $magentoVersion, $patchVersion) = array_map('trim', explode('|', $buffer));
                    $patch = array(
                        "patch" => $patchName,
                        "patchVersion" => $patchVersion,
                        "magentoVersion" => $magentoVersion
                    );
                    $this->appliedPatches[] = $patch;
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }

        // Workaround to always close the stream as "finally" block does not exist in PHP 5.4
        if (isset($ioAdapter)) {
            $ioAdapter->streamClose();
        }
    }
}
