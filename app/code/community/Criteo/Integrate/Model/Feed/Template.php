<?php

class Criteo_Integrate_Model_Feed_Template extends Mage_Core_Model_Abstract
{
    private $root;
    private $channel;

    public function __construct()
    {
        $this->root = new SimpleXMLElement('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"/>');
        $this->channel = $this->root->addChild('channel');
    }

    public function setTitle($title)
    {
        $this->channel->addChild('title', $title);
    }

    public function setLink($link)
    {
        $this->channel->addChild('link', $link);
    }

    public function setDescription($description)
    {
        $this->channel->addChild('description', $description);
    }

    public function addProduct($product, $parentProduct)
    {
        $productRootNode = $this->channel->addChild('item');
        $feedProduct = Mage::getModel('Criteo_Integrate_Feed/productTemplate', $productRootNode);
        $feedProduct->setProduct($product, $parentProduct);
    }

    public function asXML()
    {
        return $this->root->asXML();
    }
}
